// JSON - data format used to store and transfer data

// Example of JSON format:

let person = {
	"name" : "Juan",
	"weight" : 175,
	"age" : 40,
	"car" : ["Honda", "Toyota"],
	"motorbike" : {
		"brand" : "Ducati",
		"color" : "red"
	}
};

// Methods: stringify() and parse()

// stringify(), simply passes in a JS object that you want to convert to JSON
let cat = {
	name : "Mashiro",
	color : "brown",
	breed : "Siamese"	
};

let catJson = JSON.stringify(cat);
console.log(catJson);

// parse(), reverses stringify and always enclose the whole object in single quotes because it should be a string
let jsonCat = '{"name":"Mashiro","color":"brown","breed":"Siamese"}';

let cat2 = JSON.parse(jsonCat);
console.log(cat2);

/*
	What is SQL and RDBMS?
	- SQL stands for Structured Query Language
	- Programming langugae that is used to access information to Relational Database Management System.
	
	Database management system (DBMS)
	- system used to define, store, manipulate, and analyze data
	
	Samples of RDBMS: SQL Server, MySQL, Oracle, MariaDB and PostgreSQL
	
	In an RDBMS, data is stored in tables with rows and columns In each and every record, there is a primary key.
	
	Referential Integrity, rule is DB that specifies that the foreign key in one table exists as a primary key in other tables.
*/

/*
	Properties of a Relational Table (RDBMS Properties)
	
	1. Values are atomic
		atomic value property is one of the cornerstone of the relational DB.
		Address (not atomic)
			- House Number
			- Street
			- City/Municipality
			
		It should be just:
		House Number
		Street
		City/Municipality
		
	2. Column values are of the same kind.
	
	3. Each row is unique, this ensures that no two rows in a relational table are identical.
	
	4. The sequence of columns is insignificant.
	
	5. The sequence of rows is insignificant.
	
	6. Each column has a unique name.
*/

/*
	The Rise of NoSQL
	
	1. Structured Data - can be saved to the DB
	2. Unstructured Data - no definite form, some make immediate sense and some do not
	
	NoSQL stands for Not only SQL
	
	// Motivation to Develop NoSQL
	- To efficiently handle big data. V's (volume, velocity, variety and valence)
	Data Valence - Relationship of information within a data set. The higher the relationship, the more valuable.
	
	// Pros of NoSQL
	- Data structures can change without crashing
	- Data is primarily non-relational, but can imitate some features of relational data.
	- Data replication is a feature that is usually built-in to these types of databases.
	- Designed to work smoothly even in lower-spec hardware.
	
	// Entities are Objects!
	
	SQL
	Database schema (structure)
	- relationship of entities to its attributes
	- entity relationship diagram
		- one to one
		- one to many
		- many to many

		customer	1 ----- many order
		order		1 ----- 1 customer
	
	STUDENT
	
	studentID	studentName    street // Sample of Entities
	12345		Mariano		   Jeju   // Attributes describe entities
	
	STUDENT'S ADDRESS
	
	addNum		Street		island
	38			Jeju		Island
	
	
	NoSQL
	studentID		Value
	123				{	"studentName": "Juan",
						"studentLastName": "Dela Cruz",
						"address": {
							"addNum": "38",
							"street": "Jeju Island",
							"province": "Pasig"
						},
						"age": 38,
						"job": "artist"
					}
					
	// Data duplication, to securely transfer data through different servers.
	Manila > Student > Clark > Singapore
	
	Key-value store
	Document-based store //hierarchy
	Column-based store
	Graph-based store
*/

/*
	MongoDB
	- document-based database that uses key-value pairs and the leading NoSQL database.
	-language is highly expressive and generally friendly with those familiar with the JSON structure.
	
	How to design data models in MongoDB?
	- revolves around the structure of the document
	
	References, stores the relationship between the data by including links or reference from one document to another.
	
	Embedded documents, captures relationships between data by storing related data in a single document structure.
	
	Syntax:
	users = {
		categoryName: {
			type: String,
			required: [true, 'Category name is required']
		},
		description: {
			type: String,
			default: null
		}
	}
*/

/*
	MongoDB Atlas
*/